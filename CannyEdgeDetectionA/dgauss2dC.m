
% -- Generate gaussian coefficients matrix with an order user defined 
% Arguments::
%     1- order
%     2- Sigma 
%     3- desired gradiant( char 'x' or 'y')
% 

function d = dgauss2dC(order,sigma,grad)

d=0;
i=0; %#ok<NASGU>
j=0;


for x = -fix(order/2):1:fix(order/2)
    j=j+1;
    i=0;
    for y = -fix(order/2):1:fix(order/2)
        i=i+1;
        if grad == 'x'
            d(i,j) = gauss2d(x,y,sigma) * dgauss2d(x,x,y,sigma) ; %#ok<AGROW>
        elseif grad =='y'
            d(i,j) = gauss2d(x,y,sigma) * dgauss2d(y,x,y,sigma) ; %#ok<AGROW>
        end

    end
end

d = d / sqrt(sum(sum(abs(d).*abs(d))));

% -- Function of dervivative gaussian 2D equation 
% Arguments::
%     1 direction of wanted gradiant  (x or y)
%     2 X
%     3 Y 
%     4 Sigma 
%     

function fd = dgauss2d(gradient,x,y,sigma)
fd = -gradient * gauss2d(x,y,sigma) / sigma^2;



% -- Function of gaussian 2D equation 
% Arguments::
%    1- X
%    2- Y 
%    3- Sigma
% 
 
function f = gauss2d(x,y,sigma)
f = sqrt(1/2/pi) * exp(-((x^2+y^2)/(2*sigma^2))) * (1/sigma);
