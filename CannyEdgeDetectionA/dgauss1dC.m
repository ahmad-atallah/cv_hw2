% -- Generate gaussian coefficients matrix with an order user defines 
% Arguments::
%     1- order
%     2- Sigma 
%     3- desired gradiant( char 'x' or 'y')


function d = dgauss1dC(order,sigma,grad)

d=0;
i=0; 

for x = -fix(order/2):1:fix(order/2)
    i = i + 1;
        if grad == 'x'
            d(i) = gauss1d(x,y,sigma) * dgauss1d(x,x,sigma) ; %#ok<AGROW>
        elseif grad =='y'
            d(i) = gauss1d(x,y,sigma) * dgauss1d(y,x,sigma) ; %#ok<AGROW>
        end

   
end

d = d / sqrt(sum(sum(abs(d).*abs(d))));

% -- Function of dervivative gaussian 1D equation 
% Arguments::
%     1 direction of wanted gradiant  (x or y)
%     2 X
%     3 Sigma 
%     

function fd = dgauss1d(gradient,x,sigma)
fd = -gradient * gauss1d(x,sigma) / sigma^2;



% -- Function of gaussian 1D equation 
% Arguments::
%    1- X
%    2- Y 
%    3- Sigma
% 
 
function f = gauss1d(x,sigma)
f = sqrt(1/2/pi) * exp(-(x^2/(2*sigma^2))) * (1/sigma);

