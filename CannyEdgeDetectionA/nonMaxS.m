% -- Get non-maxima suppression
% Usage: image_N = nonMaxS(magnitude, angle)
% Arguments::
%     1- Norms of gradient image. 
%     2- angles of gradient image.

% Where tan θ is gradient direction; Quantization of normal directions:
% 0 : -0.4142  < tanθ <   0.4142
% 1 : -0.4142  < tanθ <   2.4142
% 2 : |tanθ|   >= 2.4142
% 3 : -2.4142  < tanθ <= -0.4142

% Author: Ahmad Atallah 


function nonMaxS_temp = nonMaxS(magnitude, angle)

if (isempty(magnitude) | isempty(angle) )  
  error('Matrices are empty ');
end

[m,n] = size(magnitude);

for i =2:m-1
    for j = 2:n-1
        
        % Precompute an matrix, Z, of index offset values that correspond to the eight 
        % neighbor pixels of any point. Z, will be:
        %               i-1,j-1   i-1,j   i-1,j+1
        %
        %               i,j-1     i,j     i,j+1
        %                     
        %               i+1,j-1   i+1,j   i+1,j+1

       % To do: change the size dynamially so it will be order * order 
       Z=[magnitude(i-1,j-1), magnitude(i-1,j), magnitude(i-1,j+1);
          magnitude(i,j-1)  , magnitude(i,j)  , magnitude(i,j+1);
	      magnitude(i+1,j-1), magnitude(i+1,j), magnitude(i+1,j+1)];
        
        % -- In direction 0
        if  -22.5 <= angle(i,j)< 22.5 
            if (Z(2,2) > Z(2,1)) && (Z(2,2) > Z(2,3))
               nonMaxS_temp(i,j)= Z(2,2);               
            else
               nonMaxS_temp(i,j)= 1; 
            end

        end 
        
        % -- In direction 1
        if 22.5 <= angle(i,j)< 67.5
            
            if (Z(2,2) > Z(1,3)) && (Z(2,2) > Z(3,1))
               nonMaxS_temp(i,j)= Z(2,2);               
            else
               nonMaxS_temp(i,j)= 0; 
            end

        end
        
        % -- In direction 2
        if angle(i,j) >= 67.5 
            
            if (Z(2,2) > Z(1,2)) && (Z(2,2) > Z(3,2))
               nonMaxS_temp(i,j)= Z(2,2);               
            else
               nonMaxS_temp(i,j)= 0; 
            end

        end
        
        % -- In direction 3
        if -67.5<= angle(i,j)<-22.5 
            
            if (Z(2,2) > Z(1,1)) && (Z(2,2) > Z(3,3))
               nonMaxS_temp(i,j)= Z(2,2);                %#ok<AGROW>
            else
               nonMaxS_temp(i,j)= 0;    %#ok<AGROW>
            end

        end
               
        
    end
end

subplot(3,2,5);

imagesc(nonMaxS_temp);
title('Non-Maximal Suppression');