function runCanny(varargin)

%--------------------------------------------------------------------------
% Tests for : Canny transform
%--------------------------------------------------------------------------
% Run the steps Sequentially

%%
clear; clc;
function badImageBorders()
order = 4; sigma = 1; alfa=0.1;
% Get the acquired image.
img = imread('cameraman1.png');
   if size(img,3)==3
        img = rgb2gray(img);
   end
figure(1);colormap(gray);
subplot(3,2,1);
imagesc(img);
colormap gray;

title('Image: Camera Man.jpg');

% -- pad image borders with enough for filter order --%
for i=1:floor(order/2)  
    
    [h,w] = size(img);
   
    img = [img(1,1)  img(1,:)  img(1,w);
            img(:,1)  img       img(:,w);
            img(h,1)  img(h,:)  img(h,w)];
end 
 imwrite(im2double(img), 'padded_image.png');
%%
function dervivativeOfGauss() 
% --- create dervivative of gaussian filter coefficient matrix ---%

fx  = dgauss2dC(order,sigma,'x'); 
fy  = dgauss2dC(order,sigma,'y'); 

% --- do the filtering in x direction ----%
image_outx = conv2(double(img),fx,'same');

% --- do the filtering in y direction ----%
image_outy = conv2(double(img),fy,'same');

subplot(3,2,2);
imagesc(image_outx);
title('Grad. X');

subplot(3,2,3);
imagesc(image_outy);
title('Grad. Y');

imwrite(uint8(image_outx), 'gradient_image_x.png');
imwrite(uint8(image_outy), 'gradient_image_y.png');

%%
function nonMaixmalSupression()
% -- Norm of the gradient (Combining the X and Y directional 
% derivatives) -- %

mag=sqrt(image_outx.*image_outx+image_outy.*image_outy);

subplot(3,2,4);
imagesc(mag);
title('Norm of Gradient');
imwrite(uint8(mag), 'norm_gradient.png');

% -- Angles of the gradient (Combining the X and Y directional
% derivatives)--%
angl = atan2(image_outy,image_outx) *(180/pi) ;
supressedImg = nonMaxS(mag,angl);


imwrite((supressedImg), 'supressed_image.png');
%%
function applyHysThreshold()

hH = hysthresh(supressedImg,100,50);
subplot(3,2,6);
imagesc(hH);
title('Hys. Threshold');

imwrite(double(hH), 'thresholded_image.png');



%%
function demoRun()
    badImageBorders();
    dervivativeOfGauss();
    nonMaixmalSupression();
    applyHysThreshold();

demoRun;



