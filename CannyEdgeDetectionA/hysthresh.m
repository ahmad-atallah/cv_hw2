% Hysteresis thresholding
%
% Usage: hT = hyshresh(locM,  Th, Tl)
%
% Arguments:
%             locM  - image to be thresholded (assumed to be non-negative)
%             Th  - upper threshold value
%             Tl  - lower threshold value
%
% Returns:
%             hT  - the thresholded image (containing values 0 or 1)


% Notes: 
% -- The output of non-maxima suppression still contains the local
% maxima created by noise.

% -- Can we get rid of them just by using a single threshold?
   %   if we set a low threshold, some noisy maxima will be accepted too.
   %   if we set a high threshold, true maxima might be missed (the value of

% -- true maxima will fluctuate above and below the threshold, fragmenting
% the edge).

% A more effective scheme is to use two thresholds:
%    1- a low threshold Tl
%    2- a high threshold Th
%    3- usually, Th =  2Tl

% This function performs hysteresis thresholding of an image.
% All pixels with values above threshold T1 are marked as edges
% All pixels that are adjacent to points that have been marked as edges
% and with values above threshold T2 are also marked as edges. Eight
% connectivity is used.
%

% Author :Ahmad Atallah



function hT = hysthresh(locM, Th, Tl)


% Check thesholds are convience or not 
if (Tl > Th | Tl < 0 | Th < 0)  
  error('T1 must be >= T2 and both must be >= 0 ');
end

% -- Representation of some parameters' values -- % 

[rows, cols] = size(locM);
hT = zeros(rows,cols);
% -- Find indices of all pixels with value > T1
[rowIs,colIs] = find (locM >= Th);   

% -- Find the number of pixels with value  > T1
nOfpix = size(rowIs,1);                

stackArray = [rowIs,colIs] ;              %Create a stack 2D array of size nOfpix(that should never
                                          % overflow!  as its row size is the size of the image)
                                          % push all the edge points on the stack
step = nOfpix;                            % set stack step
k = 1;

while(nOfpix ~= 0)
     hT( rowIs(k,1), colIs(k,1) ) = -1;   % mark points as edges
     nOfpix = nOfpix-1;
     k = k+1;
end

while (step~= 0)                           % While the stack is not empty
        v = stackArray(step,:);            % Pop next index off the stack
        step = step - 1;

        i = v(1); 
        j = v(2);
    
    % Precompute an matrix, neighbors, of index offset values that correspond to the eight 
        % neighbor pixels of any point. neighbors, will be:
        %               i-1,j-1   i-1,j   i-1,j+1
        %
        %               i,j-1     i,j     i,j+1
        %                     
        %               i+1,j-1   i+1,j   i+1,j+1
        
        % Prevent us from generating illegal indices
        % Now look at surrounding pixels to see if they
        % should be pushed onto the stack to be
        % processed as well.
        
        if i >= 2 & i < (rows -1) & j >= 2 & j < (cols-1)   
             neighbors = [  hT( i-1,j-1),   hT(i-1,j),  hT(i-1,j+1) ,...
                            hT(i,j-1)   ,   hT(i,j)  ,  hT(i,j+1) ,  ...
                            hT(i+1,j-1 ),   hT(i+1,j),  hT(i+1,j+1)];
           
            for l = 1:8
                currentN = neighbors(l);
                if  currentN > Tl                        % if value > T2,
                    [r,c] = find(hT == currentN); 
                     hT(r,c) = -1;                       % mark this as an edge point
                end
            end
         end
end


% Finally make the (negative ones) >> zeros
hT = (hT == -1);
