# README #
Name: Ahmad Atallah

### What is this repository for? ###

* Canny, Hough, and Greedy Snake Algorithims
* V1.0

### Set up ###

### 1. Canny Edge Detection 
	1.1 dguass2d function - Generate Gaussian coefficients matrix with an order (tested 3x3 coefficient)
	1.2 Convolve padded version of image with the mask.
	1.3 Apply non-maximal-suppression function.
	1.4 Apply Hysteresis for thinning (tested [100 50]).
	
### 2. Hough Transform for Lines and Circles ###
    2.1 Lines 
	 a. Test applied MATLAB canny edge detector implemented method with threshold 0.05.
	 b. The range of theta from -89 degree to 90 degree, and the range of rho is from -(l-1):l where l is the length of the image's diagonal.
	 	Then applying voting scheme and draw accumulator hough space.
     c. Hough peaks detected by setting a window and loop until the peak value is below the threshold. 
	 	If a peak which is above the threshold is found, set the value of theta and rho, 
        and draw a line with the pair of theta and rho. If there is more than 
        one peak found, we choose the first one. Suppress the window and continue loop. 
	 d- Tested Hough threshold:
	 	0.29 for first image and 0.20 for the two rest.
	 
	2.2 Circles
	  a. Test applied MATLAB canny edge detector implemented method with threshold 0.05.
	  b. Hough space accumulator optimum radius to look for(25.5).
	  c. Tested Hough threshold (0.5 * edged_image).
	  Note : Edit the range of radius according to the size of image in pixels
 	  In another words chose the radius that make sense with the image size.
	  d. Find circles after detecting peaks 
	  		(for the tested image):
	  		optimum range of radius for the tested image: 28.5 : 29 
			optimum neighborhood window: 5x5 
			optimum max_no of peaks : 20
	  e. Suppress_near circles takes sorted peaks, and a threshold of
		desired difference between two series peaks and suppress peak
		if it is near to next one. optimum value for the tested image = 7.8 
### 3. Greedy Snake ###
	3.1 Neighborhood Calculations
		- Eimg. and Eimg Normalized : location(x,y) = min - mag(x,y)/max-min 
		  In implementation, in order to compensate for this problem, the
		  minimum value min is set to max - 5 if max - min < 5.
		- Econt. and Econt. Normalized : Econt./max(neightborhoods)
		- Ecurv. and Econt. Normalized : Ecurv./max(neightborhoods)
		- calculate Ecomb by setting weights of Econt. , Ecurv., Eimg : alpha, beta, gamma respectively. 
	3.2 After That, calculate the curvature for the second time outside the neighbors. for suppressing high curvature.
	3.3 for the tested images 
		- The square image : alpha = 0.2, beta = 0.5, gamma = 1.0, and with edge image of sigma = 1.00
		- The heart image  : alpha = 0.7, beta = 0.35, gamma = 1.2, and with edge image of sigma = 1.00
	3.4 Calculate area and regular chain code.