function line_detected_img = lineFinder(orig_img, hough_img, hough_threshold)
[m, n] = size(orig_img);
l = ceil(sqrt(m * m + n * n));
dist = -l:l;
theta = (-89:90) / 180 * pi;

[h_row, h_col] = size(hough_img);

% -- First we set a window
win_x = round(h_row / 250);
win_y = round(h_col / 100);

h_m = max(hough_img(:));
obtained_threshold = hough_threshold * h_m;

line_num = 1;
rho_peaks = zeros(line_num,1);
theta_peaks = zeros(line_num,1);

% -- Then we loop until the peak value is below the threshold. If a peak 
% which is above the threshold is found, set the value of theta and rho, 
% and draw a line with the pair of theta and rho. If there is more than 
% one peak found, we choose the first one. Then we set all the Hough 
% values within the window to zero and continue in loop. --%
while find(hough_img > obtained_threshold)
    
    %First peak: maximum of hough space  
    [p, q] = find(hough_img == max(hough_img(:)));
    rho_peaks(line_num) = p(1);
    theta_peaks(line_num) = q(1);
    line_num = line_num + 1;
    slope_i = max(1, p(1) - win_x);
    intecept_i = min(h_row, p(1) + win_x);
    slope_j = max(1, q(1) - win_y);
    intercept_j = min(h_col, q(1) + win_y);
    
    %-- Supress the current window to search for another peak above the
    % threshold. 
    hough_img(slope_i:intecept_i, slope_j:intercept_j) = 0;
end

line_num = line_num - 1;
fh1 = figure();
imshow(orig_img);

% -- Cartisian parameterisation 
% Slope
c_X=ones(1, line_num);
c_Y=ones(1, line_num);

% Intercept
I_X=ones(1, line_num) * n;
I_Y=ones(1, line_num) * m;

for k = 1:line_num
    c_Y(k) = dist(rho_peaks(k)) / sin(theta(theta_peaks(k)));
    I_Y(k) = (dist(rho_peaks(k)) - m*cos(theta(theta_peaks(k))))/sin(theta(theta_peaks(k)));
    
    if abs(c_Y(k)-I_Y(k)) > n
        c_X(k) = dist(rho_peaks(k)) / cos(theta(theta_peaks(k)));
        I_X(k) = (dist(rho_peaks(k)) - n*sin(theta(theta_peaks(k))))/cos(theta(theta_peaks(k)));
        I_Y(k) = n;
        c_Y(k) = 1;      
    end
    line([c_Y(k),I_Y(k)], [c_X(k),I_X(k)], 'LineWidth',2.3, 'Color', [0, 1, 0]);
    pause(0.1); 
end

set(fh1, 'WindowStyle', 'normal');
img = getimage(fh1);
truesize(fh1, [size(img, 1), size(img, 2)]);
frame = getframe(fh1); 
line_detected_img = frame.cdata;
