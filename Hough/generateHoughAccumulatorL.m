function hough_img = generateHoughAccumulatorL(img, theta_num_bins,...
    rho_num_bins)
rho_num = length(rho_num_bins);
theta_num = length(theta_num_bins);
hough_img = zeros(rho_num, theta_num);
[i, j] = find(img);  
point_num = length(i);

cos_x= i * cos(theta_num_bins);
sin_y= j * sin(theta_num_bins);

M = round(cos_x + sin_y + (rho_num-1)/2 + 1);

for n = 1:theta_num
    for m = 1:point_num
        %Vote for every bin 
        hough_img(M(m, n), n) = hough_img(M(m, n), n) + 1;
    end
end

%Normalize
max_hough_img = max(hough_img(:));
hough_img = hough_img / max_hough_img * 255;