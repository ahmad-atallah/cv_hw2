function runHough(varargin)
%--------------------------------------------------------------------------
% Tests for : Hough transform
%--------------------------------------------------------------------------
%%
function step1getEdgeL()
image_list = {'houghL_1', 'houghL_2', 'houghL_3'};
for i = 1:length(image_list)
    img = imread([image_list{i} '.png']);
    edge_img = edge(img, 'canny', 0.05);
    % Note: The output from edge is an image of logical type.
    % Here we cast it to double before saving it.
    imwrite(im2double(edge_img), ['edge_' image_list{i} '.png']);
end

%%
function hough_accumulatorL()
img_list = {'houghL_1', 'houghL_2', 'houghL_3'};

for i = 1:length(img_list)
    img = imread(['edge_' img_list{i} '.png']);   
    [m, n] = size(img);
    l = ceil(sqrt(m^2 + n^2));
    rho_num_bins = -(l-1):l;
    theta_num_bins = (-89:90) / 180 * pi;
    hough_accumulatorL = generateHoughAccumulatorL(img,...
        theta_num_bins, rho_num_bins);
    
    % We'd like to save the hough accumulator array as an image to
    % visualize it. The values should be between 0 and 255 and the
    % data type should be uint8.
    imwrite(uint8(hough_accumulatorL), ['accumulator_' img_list{i} '.png']);
    imagesc(theta_num_bins, rho_num_bins, hough_accumulatorL);


end

%%
function line_detect()

img_list = {'houghL_1', 'houghL_2', 'houghL_3'};
hough_threshold = [0.29, 0.20, 0.20];
for i = 1:length(img_list)
    orig_img = imread([img_list{i} '.png']);
    hough_img = imread(['accumulator_' img_list{i} '.png']);
    line_img = lineFinder(orig_img, hough_img, hough_threshold(i));
    
    % The values of line_img should be between 0 and 255 and the
    % data type should be uint8.
    %
    % Here we cast line_img to uint8 if you have not done so, otherwise
    % imwrite will treat line_img as a double image and save it to an
    % incorrect result.    
    imwrite(uint8(line_img), ['line_' img_list{i} '.png']);
end

%%
function step1getEdgeC()
orig_img = imread('coins.png');
if size(orig_img,3)==3  
    orig_img = rgb2gray(orig_img); 
end
edge_img = edge(orig_img, 'canny', 0.05);

imwrite(im2double(edge_img), 'edge_c.png');

%% 
function hough_accumulatorC()

edge_img = imread('edge_c.png');
[rows,columns]=size(edge_img); 
x_num_bins = 1 : columns;
y_num_bins = 1 : rows; 
% optimum raduis here 26.3 for the tested image 
hough_accumulatorC = generateHoughAccumulatorC(edge_img,x_num_bins,...
    y_num_bins,25.5);

imwrite(uint8(hough_accumulatorC), 'accumulator_houghC.png');
imagesc(x_num_bins, y_num_bins, hough_accumulatorC);

%%
function circle_detect()

accum_img = imread('accumulator_houghC.png');

% Here we used half of maximum (edge_img) as a threshold 
threshold = 0.5 * max(accum_img(:));

% Note : Edit the range of radius according to the size of image in pixels
% In another words chose the radius that make sense with the image size

% optimum range for the tested image: 28.5 : 29 
% optimum neighborhood window: 5x5 
% optimum max_no of peaks : 20
[centers, radii] = circleFinder(accum_img,[28.5 29],20,threshold,[5 5]);
c = sortrows([centers,radii]);

% Note: supress_near circles takes sortd peaks, and a threshold of
% desired difference between two series peaks and supress peak if it is
% near to next one.
% optimum value for the tested image = 7.99 ; 

newCenters = supress_near(c,7.8);
circle_img = hough_circles_draw(orig_img,newCenters(:,1:2),newCenters(:,3));
imwrite(uint8(circle_img), 'circles_houghC.png');
%--------------------------------------------------------------------------
% 
%--------------------------------------------------------------------------

%%

function demoMATLABTricks()
demoMATLABTricksFun;


