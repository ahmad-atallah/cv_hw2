function ce =  supress_near(c,thresh_diff)
j = 1;

% Note : variable refer number of gap between two new circles
% this gap should be the supressed circles
k = 1;

for i = 1:length(c)-1
    xdiff = c(i,1)-c(i+1,1);
    ydiff = c(i,2)-c(i+1,2);
    if ( (abs(xdiff)> thresh_diff ) || (abs(ydiff)> thresh_diff))
        ce(j,1) = c(i,1);
        ce(j,2) = c(i,2);
        
        %Average of radius of near_circles supressed 
        ce(j,3) = sum(c(k:i,3))/(i-k+1);
        k = i;
        j= j+1;
    end
end