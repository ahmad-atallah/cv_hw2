function hough_img = generateHoughAccumulatorC(img,x_num_bins,...
    y_num_bins,radius)
    % Compute Hough accumulator array for finding circles.
    %
    % img: Binary (black and white) image containing edge pixels
    % radius: Radius of circles to look for, in pixels
    x_num = length(x_num_bins);
    y_num = length(y_num_bins);
    hough_img = zeros(y_num, x_num);
    % TODO: Your code here    
    for x = 1 : x_num
        for y = 1 : y_num
            if (img(y,x))
                for theta = linspace(-179, 2 * pi, 180)
                    x0 = round(x + radius * cos(theta));                
                    y0 = round(y + radius * sin(theta));
                    if (x0 > 0 && x0 <= x_num && y0 > 0 && y0 <= size(hough_img,1))
                        hough_img(y0,x0) = hough_img(y0,x0) + 1;
                    end
                end
            end
        end
    end

max_hough_img = max(hough_img(:));
hough_img = hough_img / max_hough_img * 255;