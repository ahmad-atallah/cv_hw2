function [centers, radius] = circleFinder(hough_img, radius_range,...
    numpeaks,threshold,nHoodSize)
    % Find circles in given radius range using Hough transform.
    %
    % img: Binary (black and white) image containing edge pixels
    % radius_range: Range of circle radii [min max] to look for, in pixels
    r_min = radius_range(1);
    r_max = radius_range(2);
    centers = zeros(numpeaks, 2);
    radius = zeros(size(centers,1),1);
    row_num = 0;
    for r = linspace(r_min, r_max, 5)
        temp_centers = houghC_peaks(hough_img, numpeaks, threshold,nHoodSize);
        
        if (size(temp_centers,1) > 0)
            row_num_new = row_num + size(temp_centers,1);
            centers(row_num + 1:row_num_new,:) = temp_centers;
            radius(row_num + 1:row_num_new) = r;
            row_num = row_num_new;       
        end
    end
    centers = centers(1:row_num,:);
    radius = radius(1:row_num);
end