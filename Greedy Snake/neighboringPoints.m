function [ Z ] = neighboringPoints( x_current , y_current )
%NEIGHBORING_POINTS Summary of this function goes here
%   input : x_current - x co-ordinate point that centers the 8 neighbors
%           y_current - y co-ordinate point that centers the 8 neighbors
%   output: Z - Vector containing the co-ordinates of the 8 neighbors
%   ordered from top left corner all the way to the bottom right corner
%   sequentially.
%
%   Detailed explanation goes here
%   Finding the 8 neighbors surrounding a point
Z = [   x_current-1 y_current-1; 
        x_current-1 y_current ; 
        x_current-1 y_current+1;
        x_current y_current-1; 
        x_current y_current;
        x_current y_current+1; 
        x_current+1 y_current-1;
        x_current+1 y_current; 
        x_current+1 y_current+1 ];
    
end

