% Arguments::
%     1- Array of points
%    

% The slope between every two points of the contour array of points:
% 0 : 0      <=  θ   <=  22.5 & 337.5 <  θ  <= 360
% 1 : 22.5   <   θ   <=  67.5 
% 2 : 67.5   <   θ   <=  112.5
% 3 : 112.5  <   θ   <=  157.5
% 4 : 157.5  <   θ   <=  202.5
% 5 : 202.5  <=  θ   <=  247.5
% 6 : 247.5  <   θ   <=  292.5
% 7 : 292.5  <   θ   <=  337.5


% Author: Ahmad Atallah 


function [chainCode] = getChainCode(points)


chainCode = '';
for i = 1:length(points)-1
    theta = atan2(points(i,2)-points(i+1,2),...
        points(i,1)-points(i+1,1) )*180/pi;
    
    if i == length(points)-1
        theta = atan2(points(i,2)-points(i+1,2),...
        points(1,1)-points(1,1) )*180/pi;
    end
    
    if (0 <= theta && theta <= 22.5) || (337.5 < theta && theta <= 360)
        chainCode = strcat(chainCode,'0');
    elseif (22.5 < theta && theta <= 67.5)
        chainCode = strcat(chainCode,'1');
    elseif (67.5 < theta && theta <= 112.5)
        chainCode = strcat(chainCode,'2');
    elseif (112.5 < theta && theta <= 157.5)
        chainCode = strcat(chainCode,'3');
    elseif (157.5 < theta && theta <= 202.5)
         chainCode = strcat(chainCode,'4');
    elseif (202.5 < theta && theta <= 247.5)
         chainCode = strcat(chainCode,'5');
    elseif (247.5 < theta && theta <= 292.5)
         chainCode = strcat(chainCode,'6');
         
    else 
        chainCode = strcat(chainCode,'7');
    end
            
               
    
end

end
