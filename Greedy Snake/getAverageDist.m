%AVERAGE_DISTANCE Summary of this function goes here

% input: xy - matrix that contains the co-ordinates of my contour points
% output: average_d - average distance 

%   Detailed explanation goes here
% Calculating average distance between the contour points
% Calculating the distance between each point and the next one
% and then adding all the distances and dividing by the number of points to
% get the mean

function [averageDist] = getAverageDist(xy,n)


for i = 1 : n
    
    if(i == n)
        x_2 = xy(1,1);
        y_2 = xy(1,2);
        x_1 = xy(i,1);
        y_1 = xy(i,2);
    else
           
        x_1 = xy(i,1);
        x_2 = xy(i+1,1);
        y_1 = xy(i,2);
        y_2 = xy(i+1,2); 
    end

    
    x_distance = (x_2 - x_1)^2;
    y_distance = (y_2 - y_1)^2;
    distance(i,1) = (x_distance + y_distance)^ 0.5;
    averageDist= sum(distance) / n;
    
    
end


end

% sum = 0;
% for i = 1:n
%     sum = sum + norm(points(1:2, i) - points(1:2, mod(i,n)+1));
% end
% 
% averageDist = sum/n