%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% greedySnake.m is the GUI frontend for a program which demonstrates the active
% contour (greedy snakes) model.

function varargout = greedySnake(varargin)
% GREEDYSNAKE M-file for greedySnake.fig

% Begin initialization code - 
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @greedySnake_OpeningFcn, ...
                   'gui_OutputFcn',  @greedySnake_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before greedySnake is made visible.
function greedySnake_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to greedySnake (see VARARGIN)

% Choose default command line output for greedySnake
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

%default initial values for various parameters
set(handles.edit9,'string','1.00');
set(handles.edit5,'string','0.40');
set(handles.edit6,'string','0.20');
set(handles.edit7,'string','1.00');
set(handles.edit10,'string','200');

%Housekeeping
set(handles.edit5,'Enable','off');
set(handles.edit6,'Enable','off');
set(handles.edit7,'Enable','off');
set(handles.edit10,'Enable','off');
set(handles.pushbutton2,'Enable','off');

% UIWAIT makes greedySnake wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = greedySnake_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double



% --------------------------------------------------------------------
function img_Callback(hObject, eventdata, handles)
% hObject    handle to img (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

sigma_val = str2double(get(handles.edit9,'String')); % gets sigma from text box
y = filter_function(handles.image,sigma_val); % smooths the image
axes(handles.axes4)
imshow(y,[]);
% intializing the greedySnake on the image
[xs, ys] = getsnake(y);
points = [xs;ys];
handles.smth = y;
handles.points = points;
handles.xs = xs;
handles.ys = ys;
guidata(hObject, handles);

% Housekeeping
set(handles.edit5,'Enable','on');
set(handles.edit6,'Enable','on');
set(handles.edit7,'Enable','on');
set(handles.edit9,'Enable','off');
set(handles.edit10,'Enable','on');
set(handles.pushbutton1,'Enable','off');
set(handles.pushbutton2,'Enable','on');


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%obtiaing various paramaeters form the text boxes
alpha_val = str2double(get(handles.edit5,'String'));
beta_val = str2double(get(handles.edit6,'String'));
gamma_val = str2double(get(handles.edit7,'String'));
maxIterations = str2double(get(handles.edit10,'String'));


% set(handles.pushbutton2,'Enable','off');
% Making the greedySnake move
[pts,area]= greedySnk(handles.smth, handles.points, alpha_val, beta_val, gamma_val, maxIterations);
finalArea = area(length(area));
finalpts  = pts{length(pts)};
chainCode = getChainCode(finalpts); 
set(handles.edit17,'string',num2str(finalArea));
set(handles.edit16,'string',(chainCode));

set(handles.pushbutton2,'Enable','on');

set(handles.pushbutton1,'Enable','on');
set(handles.pushbutton3,'Enable','on');
set(handles.pushbutton4,'Enable','on');

set(handles.edit17,'Enable','off');
set(handles.edit16,'Enable','off');



% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename, pathname] = uigetfile(...
    {'*.tif;*.jpg;*.pgm';'*.*'},'File Selector');
handles.filename = strcat(pathname,filename);
guidata(hObject, handles);
handles.filename

axes(handles.axes4)
x = imread(handles.filename);
if size(x,3)==3
    x = rgb2gray(x);
end 
imshow(x,[]);

handles.image = x;
guidata(hObject, handles);

% Following code set the parameters according to the image selected
if(strcmp(filename,'img1.tif'))
    set(handles.edit9,'string','2.00');
    set(handles.edit5,'string','0.40');
    set(handles.edit6,'string','0.20');
    set(handles.edit7,'string','0.50');
    
    set(handles.edit10,'string','200');
    
elseif(strcmp(filename,'img2.tif'))
    set(handles.edit9,'string','1.00');
    set(handles.edit5,'string','0.40');
    set(handles.edit6,'string','0.20');
    set(handles.edit7,'string','0.40');
  
    set(handles.edit10,'string','300');
    
elseif(strcmp(filename,'square.jpg'))
    set(handles.edit9,'string','1.00');
    set(handles.edit5,'string','0.20');
    set(handles.edit6,'string','0.20');
    set(handles.edit7,'string','0.50');
    
    set(handles.edit10,'string','450');
    
elseif(strcmp(filename,'circle.jpg'))
    set(handles.edit9,'string','1.00');
    set(handles.edit5,'string','0.40');
    set(handles.edit6,'string','0.20');
    set(handles.edit7,'string','0.50');
   
    
    set(handles.edit10,'string','200');
elseif(strcmp(filename,'heart.pgm'))
    set(handles.edit9,'string','2.00');
    set(handles.edit5,'string','0.10');
    set(handles.edit6,'string','0.05');
    set(handles.edit7,'string','0.70');
   
    set(handles.edit10,'string','200');
elseif(strcmp(filename,'room.pgm'))
    set(handles.edit9,'string','1.00');
    set(handles.edit5,'string','0.40');
    set(handles.edit6,'string','0.20');
    set(handles.edit7,'string','0.60');
   
    
    set(handles.edit10,'string','200');
elseif(strcmp(filename,'new.pgm'))
    set(handles.edit9,'string','5.00');
    set(handles.edit5,'string','0.20');
    set(handles.edit6,'string','0.20');
    set(handles.edit7,'string','0.70');
   
    set(handles.edit10,'string','200');
elseif(strcmp(filename,'chest.pgm'))
    set(handles.edit9,'string','1.00');
    set(handles.edit5,'string','0.10');
    set(handles.edit6,'string','0.10');
    set(handles.edit7,'string','0.50');
   
    
    set(handles.edit10,'string','200');
end

%housekeeping
set(handles.edit5,'Enable','off');
set(handles.edit6,'Enable','off');
set(handles.edit7,'Enable','off');
set(handles.edit9,'Enable','on');
set(handles.edit10,'Enable','off');
set(handles.pushbutton2,'Enable','off');
set(handles.pushbutton1,'Enable','on');


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

axes(handles.axes4)
imshow(handles.image,[]);

% housekeeping
set(handles.edit5,'Enable','off');
set(handles.edit6,'Enable','off');
set(handles.edit7,'Enable','off');
set(handles.edit9,'Enable','on');
set(handles.edit10,'Enable','off');

set(handles.pushbutton2,'Enable','off');
set(handles.pushbutton1,'Enable','on');



function edit16_Callback(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit16 as text
%        str2double(get(hObject,'String')) returns contents of edit16 as a double


% --- Executes during object creation, after setting all properties.
function edit16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------



function edit17_Callback(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit17 as text
%        str2double(get(hObject,'String')) returns contents of edit17 as a double


% --- Executes during object creation, after setting all properties.
function edit17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
