function [ point , previous_point , next_point ] = getPlusMinus( index  , maximum )
%GET_P_N Summary of this function goes here
%   input: current_index - the input point i am currently on from my
%   contour points
%          maximum - the maximum number of contour points
%   output: point - current index inputed
%           previous_point - current index - 1
%           next_point - current index +1
%   Detailed explanation goes here
%   This code fixes the 2 extreme cases where we have the current index
%   equals to "1" and so the output should be n ( previous index ) and 2 (
%   next index ) and the other extreme case when the current index equals
%   to "maximum" and so the output should be maximum - 1 ( previous index)
%   and 1 ( next index ).
%   This happens because the snake is a closed contour.

point = mod(index,maximum);
    if point == 0
        point = maximum;
    end
previous_point = point - 1;
next_point = point + 1;
    if previous_point == 0
        previous_point = maximum;
    end
    if next_point > maximum
        next_point = 1;
    end

end

