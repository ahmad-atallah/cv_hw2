function [updatedPoints,area] = greedySnake(image, points, alpha, beta, gamma,maxIteration)

%Curvature threshold
cThreshold = 0.3;

% Image energy term threshold
imgEnrgThresh = 120;

%iterator  
itCounter = 0;
updatedPoints = {1,itCounter};
area = zeros(1,itCounter);
%Rounding indices of manual selected snake points
points(1:2,:) = round(points(1:2,:));
points = points';

%Calculating the image energy
energyImg = getImageEnrg(image);

% Get number of points in the snake
n = size(points, 1);

% Get average distance between points
avgDist = getAverageDist(points, n);


% Preallocations for increased code efficiency
Econt = zeros(9,1);
Ecurv = zeros(9,1);
EimgEnrgT = zeros(9,1);

neighborStore = zeros(n,1);
EcurvA = zeros(n,1);

bool = true;
while bool
    pointsShrinked = 0;
%     p = randperm(n);
    for i = 1 : n %p(1:n) 
        
                   
        % get previous and next points of current one 
        [current , previous , next ] = getPlusMinus( i , n ) ;
        
        x_current   = points(current,1);
        y_current   = points(current,2);
        x_previous  = points(previous,1);
        y_previous  = points(previous,2);
        x_next      = points(next,1);
        y_next      = points(next,2);
        
        % Find the 8 neighbors surrounding the current contour point.
        xy_current_neighbors = neighboringPoints(x_current, y_current);

        % Calculate energy term for the neighboroods
        
%         neighborsEnrg = zeros(9,1);
%         for k = 1: 9
%             neighborsEnrg(k,1) = 
%         end
        
        % normalize energy term 
        
        for j = 1 : 9
        
        % Extracting x and y co-ordinates of the neighbor points
        x_neighbor = xy_current_neighbors(j,1);
        y_neighbor = xy_current_neighbors(j,2);
        
        % Calculating the distance between the neighbors and the previous
        % contour point - Continuity
        x_distance = ( x_neighbor - x_previous ) ^2 ;
        y_distance = ( y_neighbor - y_previous ) ^2 ;
        
        distance = ( x_distance + y_distance ) ^0.5;
        Econt(j,1) = abs(avgDist - distance);
        
        % Calculating the distance between the neighbors and the previous
        % contour point - Curvature 
        xCurv = x_next - 2*x_neighbor + x_previous;
        yCurv = y_next - 2*y_neighbor + y_previous;
        Ecurv(j,1) = (xCurv^2 + yCurv^2);
        
        
        % The image energy term
        EimgEnrgT(j,1) = energyImg(xy_current_neighbors(j,1),xy_current_neighbors(j,2));
        
        end
    
    
    % Normalize the continuity and curvature terms to lie in the range [0,1]
    Econt = Econt / max(Econt); %check normalization
    Ecurv = Ecurv / max(Ecurv);
    EimgEnrgT = normalizeEnrgTerm(EimgEnrgT);

    % Sum the energy terms
    Esnake = alpha*Econt + beta*Ecurv + gamma*EimgEnrgT;
    
    [~,minIndex] = min(Esnake);
    
    %check that the min not the current postion
    if minIndex ~= 5 
        
        %Move point to new location 
        points(i,1) = xy_current_neighbors(minIndex,1);
        points(i,2) = xy_current_neighbors(minIndex,2);

        pointsShrinked = pointsShrinked+1;
        
    end
    neighborStore(i,1) = EimgEnrgT(minIndex,1);


    end
    
    % Iterate through all snake points to find curvatures
    for i = 1:n

    [current , previous , next ] = getPlusMinus( i , n ) ;
    % Estimate the curvature at each point
     x_current   = points(current,1);
     y_current   = points(current,2);
     x_previous  = points(previous,1);
     y_previous  = points(previous,2);
     x_next      = points(next,1);
     y_next      = points(next,2);
        
    uix =  x_current - x_previous;
    uiy =  y_current - y_previous;

    uiPlusx = x_next - x_current;
    uiPlusy = y_next - y_current;
    ui = sqrt(uix^2 + uiy^2);
    uiPlus = sqrt(uiPlusx^2+uiPlusy^2);
    
    uixx = uix/ui;
    uixy = uiy/ui;
    
    uiPlusxx = uiPlusx/uiPlus;
    uiPlusxy = uiPlusy/uiPlus;
    
    uifinalx = uixx - uiPlusxx;
    uifinaly = uixy - uiPlusxy;
    EcurvA(i) = uifinalx^2 + uifinaly^2;
    end
    
    % Iterate through all snake points to find where to relax beta
    for i = 1:n
    %   Use modulo arithmetic since the curve is closed
        [current , previous , next ] = getPlusMinus( i , n ) ;
        %   Estimate the curvature at each point
                
        
        if ( EcurvA(current) > EcurvA(previous) && ...
        EcurvA(current) > EcurvA(next) && ...
        EcurvA(current) > cThreshold && ...
        neighborStore(current) > imgEnrgThresh && ...
        beta ~= 0 )
    
        beta = 0;
        disp(['Relaxed beta for point nr. ', num2str(i)]);
        end
    end
    itCounter = itCounter+1;
    updatedPoints(itCounter) = {points};
    imshow(image,[]); 
    hold on;
    plot([points(:,1);points(1,1)],[points(:,2);points(1,2)],'r-');
    area(itCounter) = polyarea([points(:,1);points(1,1)],[points(:,2);points(1,2)]);
    hold off;
    pause(0.001)    


    if (itCounter == maxIteration || pointsShrinked < 3)
        bool = false;
        updatedPoints =  updatedPoints(1:itCounter);
    end
    avgDist = getAverageDist(points, n);
    
end 
