function [ normalized_vector ] = normalizeEnrgTerm( vector )

%   Summary of this function goes here
%   input : vector - the vector to be normalized
%   output : normalized_vector - the output normalized vector
%   Detailed explanation goes here
%   Min-Max normalization , setting a vector's values between [ 0 , 1 ]


minEnrg = min(vector);
maxEnrg = max(vector);
normalized_vector = zeros (size(vector,1),1);

if (maxEnrg - minEnrg < 5)
       minEnrg = maxEnrg - 5;
end
        
num_rows = size(vector,1);

for i=1:num_rows
    
    normalized_vector(i,1) = (vector(i,1) - minEnrg) / (maxEnrg - minEnrg);
  
end


