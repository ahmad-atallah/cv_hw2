function [enrgImage] = getImageEnrg(gaussianImage)

fy = fspecial('sobel');
fx = fy';
gx = imfilter(gaussianImage,fx,'replicate');
gy = imfilter(gaussianImage,fy,'replicate');

enrgImage = sqrt(gx.^2 + gy.^2);